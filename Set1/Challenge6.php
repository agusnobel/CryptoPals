<?php

namespace CryptoPals\Set1;

use CryptoPals\Challenge;

class Challenge6 extends Challenge
{
    const BASE64_FILE = 'https://www.cryptopals.com/static/challenge-data/6.txt';

    /**
     * Note: This is not the most efficient way to solve this. This is a representation of my solution by hand.
     * Most is done in strings so it's easy to understand. Follow all functions to see how every part works.
     */
    public static function solve()
    {
        for($i = 2; $i <= 40; $i++) {
            $binaryString = Challenge5::asciiToBinary(chr($i));

            var_dump($binaryString);
            // TODO: Finish this challenge
        }
    }

    public static function hammingDistance($stringA, $stringB)
    {
        $binaryStringA = Challenge5::asciiToBinary($stringA);
        $binaryStringB = Challenge5::asciiToBinary($stringB);

        $xorCombination = Challenge2::fixedXOR($binaryStringA, $binaryStringB);

        $distance = 0;

        for($i = 0; $i < strlen($xorCombination); $i++) {
            $distance += intval($xorCombination[$i]);
        }

        return $distance;
    }
}