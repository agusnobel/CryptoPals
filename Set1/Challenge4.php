<?php

namespace CryptoPals\Set1;

use CryptoPals\Challenge;

class Challenge4 extends Challenge
{
    const STRINGS_FILE = 'https://www.cryptopals.com/static/challenge-data/4.txt';

    /**
     * Note: This is not the most efficient way to solve this. This is a representation of my solution by hand.
     * Most is done in strings so it's easy to understand. Follow all functions to see how every part works.
     */
    public static function solve()
    {
        $strings = file(self::STRINGS_FILE, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        $highScore = null;

        foreach($strings as $string) {
            $result = Challenge3::findHighScoringSingleByteXORChipher($string);

            if($highScore === null || $highScore->score < $result->score) {
                $highScore = $result;
            }
        }

        echo sprintf("%s is solved: Key: %s, Score: %s, Text: %s\n", self::getClassName(), $highScore->key, $highScore->score, $highScore->text);
    }
}