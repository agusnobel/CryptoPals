<?php

namespace CryptoPals\Set1;

use CryptoPals\Challenge;

class Challenge5 extends Challenge
{
    const TEXT = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
    const KEY = 'ICE';
    const SHOULD_PRODUCE = '0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f';

    /**
     * Note: This is not the most efficient way to solve this. This is a representation of my solution by hand.
     * Most is done in strings so it's easy to understand. Follow all functions to see how every part works.
     */
    public static function solve()
    {
        $keys = [];

        for($i = 0; $i < strlen(self::KEY); $i++) {
            $binaryChar = decbin(ord(self::KEY[$i]));

            while(strlen($binaryChar) < 8) {
                $binaryChar = '0' . $binaryChar;
            }

            $keys[] = $binaryChar;
        }

        $binaryString = self::asciiToBinary(self::TEXT);

        $binaryKeyString = '';
        $keyCycle = 0;

        while(strlen($binaryKeyString) < strlen($binaryString)) {
            if($keyCycle >= strlen(self::KEY)) {
                $keyCycle = 0;
            }

            $binaryKeyString .= $keys[$keyCycle];
            $keyCycle++;
        }

        $xorCombination = Challenge2::fixedXOR($binaryString, $binaryKeyString);
        $hexString = base_convert_arbitrary($xorCombination, 2, 16);

        while(strlen($hexString) % 2 > 0) {
            $hexString = '0' . $hexString;
        }

        echo sprintf("%s is%s solved\n", self::getClassName(), $hexString === self::SHOULD_PRODUCE ? '' : ' NOT');
    }

    public static function asciiToBinary($string)
    {
        $binaryString = '';

        for($i = 0; $i < strlen($string); $i++) {
            $binaryChar = decbin(ord($string[$i]));

            while(strlen($binaryChar) < 8) {
                $binaryChar = '0' . $binaryChar;
            }

            $binaryString .= $binaryChar;
        }

        return $binaryString;
    }
}