<?php

namespace CryptoPals\Set1;

use CryptoPals\Challenge;

class Challenge3 extends Challenge
{
    const HEX_STRING = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736';

    /**
     * Note: This is not the most efficient way to solve this. This is a representation of my solution by hand.
     * Most is done in strings so it's easy to understand. Follow all functions to see how every part works.
     */
    public static function solve()
    {
        $highScore = self::findHighScoringSingleByteXORChipher(self::HEX_STRING);

        echo sprintf("%s is solved: Key: %s, Score: %s, Text: %s\n", self::getClassName(), $highScore->key, $highScore->score, $highScore->text);
    }

    public static function findHighScoringSingleByteXORChipher($hexString)
    {
        $binaryString = Challenge1::hexToBinary($hexString);
        $possibleBytesArray = sampling([0,1], 8);

        $highScore = null;

        foreach($possibleBytesArray as $possibleByte) {
            $binaryKeyString = '';

            while(strlen($binaryKeyString) < strlen($binaryString)) {
                $binaryKeyString .= $possibleByte;
            }

            $xorCombination = Challenge2::fixedXOR($binaryString, $binaryKeyString);
            $string = '';

            for($i = 0; $i < strlen($xorCombination) / 8; $i++) {
                $byte = substr($xorCombination, $i * 8, 8);
                $string .= chr(base_convert_arbitrary($byte, 2, 10));
            }

            $result = new \stdClass();
            $result->key = $possibleByte;
            $result->score = scoreASCII($string);
            $result->text = $string;

            if($highScore === null || $highScore->score < $result->score) {
                $highScore = $result;
            }
        }

        return $highScore;
    }
}