<?php

namespace CryptoPals\Set1;

use CryptoPals\Challenge;

class Challenge1 extends Challenge
{
    const THE_STRING = '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d';
    const SHOULD_PRODUCE = 'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t';

    const BASE64_CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    /**
     * Note: This is not the most efficient way to solve this. This is a representation of my solution by hand.
     * Most is done in strings so it's easy to understand. Follow all functions to see how every part works.
     */
    public static function solve()
    {
        $base64 = self::hexToBase64(self::THE_STRING);

        echo sprintf("%s is%s solved\n", self::getClassName(), $base64 === self::SHOULD_PRODUCE ? '' : ' NOT');
    }

    /**
     * @param string $hexString
     * @return string
     */
    public static function hexToBase64($hexString)
    {
        // Convert HEX to binary
        $binary = self::hexToBinary($hexString);

        // Convert binary to 6-bit array
        $bitArray = self::binaryToBitArray($binary, 6);

        // Convert 6-bit array to decimal array
        $decimalArray = self::bitArrayToDecimalArray($bitArray);

        // Convert decimalArray to base64
        $base64 = self::decimalArrayToBase64($decimalArray);

        return $base64;
    }

    public static function hexToBinary($hexString)
    {
        if(strlen($hexString) % 2 != 0) {
            throw new \InvalidArgumentException('Input is not a valid hex value.');
        }

        return base_convert_arbitrary($hexString, 16, 2);
    }

    public static function binaryToBitArray($binaryString, $bitLength)
    {
        // Add padding, so there are enough bits to divide
        while(strlen($binaryString) % $bitLength != 0) {
            $binaryString = '0' . $binaryString;
        }

        $bitArray = [];

        // Get pairs from the binaryString and add them to the bitArray
        for($i = 0; $i < strlen($binaryString) / $bitLength; $i++) {
            $bitArray[] = substr($binaryString, $i * $bitLength, $bitLength);
        }

        return $bitArray;
    }

    public static function bitArrayToDecimalArray($bitArray)
    {
        $decimalArray = [];

        // Convert all bits to decimals and add them to the decimalArray
        foreach($bitArray as $index => $bits) {
            $decimalArray[$index] = base_convert($bits, 2, 10);
        }

        return $decimalArray;
    }

    public static function decimalArrayToBase64($decimalArray)
    {
        $base64 = '';

        foreach($decimalArray as $decimal) {
            $base64 .= substr(self::BASE64_CHARSET, $decimal, 1);
        }

        while(strlen($base64) % 4 > 0) {
            $base64 .= '=';
        }

        return $base64;
    }
}