<?php

namespace CryptoPals\Set1;

use CryptoPals\Challenge;

class Challenge2 extends Challenge
{
    const FIRST_HEX = '1c0111001f010100061a024b53535009181c';
    const SECOND_HEX = '686974207468652062756c6c277320657965';
    const SHOULD_PRODUCE = '746865206b696420646f6e277420706c6179';

    /**
     * Note: This is not the most efficient way to solve this. This is a representation of my solution by hand.
     * Most is done in strings so it's easy to understand. Follow all functions to see how every part works.
     */
    public static function solve()
    {
        $firstBinaryString = Challenge1::hexToBinary(self::FIRST_HEX);
        $secondBinaryString = Challenge1::hexToBinary(self::SECOND_HEX);

        // XOR the two binaryStrings
        $xorCombination = self::fixedXOR($firstBinaryString, $secondBinaryString);

        // Convert binaryString to HEX
        $xorCombinationHex = base_convert_arbitrary($xorCombination, 2, 16);

        echo sprintf("%s is%s solved\n", self::getClassName(), $xorCombinationHex === self::SHOULD_PRODUCE ? '' : ' NOT');
    }

    public static function fixedXOR($binaryStringA, $binaryStringB)
    {
        // Add padding if A is shorter than B
        while(strlen($binaryStringA) < strlen($binaryStringB)) {
            $binaryStringA = '0' . $binaryStringA;
        }

        // Add padding if B is shorter than A
        while(strlen($binaryStringA) > strlen($binaryStringB)) {
            $binaryStringB = '0' . $binaryStringB;
        }

        $xorCombination = '';

        for($i = 0; $i < strlen($binaryStringA); $i++) {
            $xorCombination .= $binaryStringA[$i] == $binaryStringB[$i] ? '0' : '1';
        }

        return $xorCombination;
    }
}