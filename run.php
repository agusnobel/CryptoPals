<?php

require_once 'functions.php';
require_once 'Challenge.php';
require_once 'Set1/Challenge1.php';
require_once 'Set1/Challenge2.php';
require_once 'Set1/Challenge3.php';
require_once 'Set1/Challenge4.php';
require_once 'Set1/Challenge5.php';
require_once 'Set1/Challenge6.php';

\CryptoPals\Set1\Challenge1::solve();
\CryptoPals\Set1\Challenge2::solve();
\CryptoPals\Set1\Challenge3::solve();
\CryptoPals\Set1\Challenge4::solve();
\CryptoPals\Set1\Challenge5::solve();
\CryptoPals\Set1\Challenge6::solve();