<?php

namespace CryptoPals;

abstract class Challenge
{
    public static function solve(){}

    protected static function getClassName()
    {
        $path = explode('\\', static::class);
        return array_pop($path);
    }
}