<?php

/**
 * base_convert_arbitrary function that fixes the limits of base_convert
 * Source: https://stackoverflow.com/questions/5301034/how-to-generate-random-64-bit-value-as-decimal-string-in-php/5302533#5302533
 * @param $number
 * @param $fromBase
 * @param $toBase
 * @return string
 */
function base_convert_arbitrary($number, $fromBase, $toBase) {
    $digits = '0123456789abcdefghijklmnopqrstuvwxyz';
    $length = strlen($number);
    $result = '';

    $nibbles = array();
    for ($i = 0; $i < $length; ++$i) {
        $nibbles[$i] = strpos($digits, $number[$i]);
    }

    do {
        $value = 0;
        $newlen = 0;
        for ($i = 0; $i < $length; ++$i) {
            $value = $value * $fromBase + $nibbles[$i];
            if ($value >= $toBase) {
                $nibbles[$newlen++] = (int)($value / $toBase);
                $value %= $toBase;
            }
            else if ($newlen > 0) {
                $nibbles[$newlen++] = 0;
            }
        }
        $length = $newlen;
        $result = $digits[$value].$result;
    }
    while ($newlen != 0);
    return $result;
}

/**
 * sampling function that simplifies getting all possible combinations of characters
 * Source: https://stackoverflow.com/questions/19067556/php-algorithm-to-generate-all-combinations-of-a-specific-size-from-a-single-set
 * @param $chars
 * @param $size
 * @param array $combinations
 * @return array
 */
function sampling($chars, $size, $combinations = array()) {

    # if it's the first iteration, the first set
    # of combinations is the same as the set of characters
    if (empty($combinations)) {
        $combinations = $chars;
    }

    # we're done if we're at size 1
    if ($size == 1) {
        return $combinations;
    }

    # initialise array to put new values in
    $new_combinations = array();

    # loop through existing combinations and character set to create strings
    foreach ($combinations as $combination) {
        foreach ($chars as $char) {
            $new_combinations[] = $combination . $char;
        }
    }

    # call same function again for the next iteration
    return sampling($chars, $size - 1, $new_combinations);

}

/**
 * Letter frequency scoring function
 * Source: https://github.com/lt/php-cryptopals/blob/master/Cryptopals/Set1/Challenge3/SingleByteXORScore.php
 */

// https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_letters_in_the_English_language
const FREQ_EN = [
    'e' => 12.702, 't' => 9.056, 'a' => 8.167, 'o' => 7.507, 'i' => 6.966, 'n' => 6.749,
    's' =>  6.327, 'h' => 6.094, 'r' => 5.987, 'd' => 4.253, 'l' => 4.025, 'c' => 2.782,
    'u' =>  2.758, 'm' => 2.406, 'w' => 2.361, 'f' => 2.228, 'g' => 2.015, 'y' => 1.974,
    'p' =>  1.929, 'b' => 1.492, 'v' => 0.978, 'k' => 0.772, 'j' => 0.153, 'x' => 0.150,
    'q' =>  0.095, 'z' => 0.074,
    /*
     * More from https://en.wikipedia.org/wiki/Letter_frequency
     *
     * "In English, the space is slightly more frequent than the top letter (e)
     * and the non-alphabetic characters (digits, punctuation, etc.) collectively
     * occupy the fourth position, between t and a."
     *
     * Made up values based on the above. Lots of "etc." unaccounted for.
     */
    ' ' => 13
];
// https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_the_first_letters_of_a_word_in_the_English_language
const FREQ_START_EN = [
    't' => 16.671, 'a' => 11.602, 's' => 7.755, 'h' => 7.232, 'w' => 6.753, 'i' => 6.286,
    'o' =>  6.264, 'b' =>  4.702, 'm' => 4.383, 'f' => 3.779, 'c' => 3.511, 'l' => 2.705,
    'd' =>  2.670, 'p' =>  2.545, 'n' => 2.365, 'e' => 2.007, 'g' => 1.950, 'r' => 1.653,
    'y' =>  1.620, 'u' =>  1.487, 'v' => 0.649, 'j' => 0.597, 'k' => 0.590, 'q' => 0.173,
    'z' =>  0.034, 'x' =>  0.017
];

function scoreASCII($data, $overrideWeights = [])
{
    $data = strtolower($data);
    $dataLen = strlen($data);
    $score = 0;
    for ($i = 0; $i < $dataLen; $i++) {
        $c = $data[$i];
        // Control characters or extended ASCII
        if (($c < ' ') || ($c > '~')) {
            $score -= 100;
            continue;
        }
        // First letter of the string, or preceeded by space or tab
        $weights = $overrideWeights + (($i === 0 || $data[$i - 1] === ' ' || $data[$i - 1] === "\x9") ?
                FREQ_START_EN : FREQ_EN);
        if (isset($weights[$c])) {
            $score += $weights[$c];
        }
        else {
            $score -= 3;
        }
    }
    // Normalise
    return $score / $dataLen;
}

function score($input, array $overrideWeights = [])
{
    $inputLen = strlen($input);
    $scores = [];
    for ($i = 0; $i < 256; $i++) {
        $trial = $input ^ str_repeat(chr($i), $inputLen);
        $scores[$i] = scoreASCII($trial, $overrideWeights);
    }
    return $scores;
}